# Resources

[1] Bundesministerium für Klimaschutz, Umwelt, Energie, Mobilität, Innovation und Technologie (BMK), "Energie in Österreich: Zahlen, Daten, Fakten", 2020: [Energie_in_IE_2020_ua.pdf](https://www.bmk.gv.at/dam/jcr:f0bdbaa4-59f2-4bde-9af9-e139f9568769/Energie_in_OE_2020_ua.pdf)
