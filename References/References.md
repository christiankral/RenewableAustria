# References

## CO2 refereneces

### Forests

- Carbon storage of trees [varies considerably between regions](https://www.nrs.fs.fed.us/pubs/gtr/gtr_wo059.pdf) and can be estimated as [0.5 kg CO2 / (m² * year)](https://www.carbonindependent.org/76.html)

### Humans

- A human exhausts approximately [0.9 kg CO2 / day](https://www.globe.gov/explore-science/scientists-blog/archived-posts/sciblog/2008/08/11/release-of-carbon-dioxide-by-individual-humans/comment-page-1/index.html), equal to approximately 1/3 ton CO2 / year
- An area of approximately 666 m² is thus required to compensate the CO2 exhaust of one human

## Fuel references

### Diesel

- The energy density of Diesel equals [10.4 kWh / liter](https://www.dlr.de/blogs/desktopdefault.aspx/tabid-6192/10184_read-252/)
- An older passenger car with an energy consumption of 9.6 liter / 100 km therefore consumes approximately 100 kWh / 100 km
- Diesel fuel consumption causes [2.64 kg CO2 / liter](https://ecoscore.be/en/info/ecoscore/co2) ≈ 2.5 kg CO2 / liter
- Driving 13000 km / per year thus causes 33 tons CO2 / year
- Very roughly estimated: 100 humans cause as many CO2 as one passenger car
